 //<>//
class Bullet {
  PImage[] bullet;
  int imageCount;
  int frame;  
  PVector velocity;
  protected PVector shotVelocity;
  private PVector bulletPos;
  boolean shot;

  Bullet() {
    imageCount = 1;
    bullet = new PImage[imageCount];
    velocity = zeroVelocity;
    shotVelocity = new PVector(0, -3);
    bulletPos = new PVector(-100, -100 );


    for (int i = 0; i < imageCount; i++) {
      // Use nf() to number format 'i' into four digits
      String filename = FINAL_ASSET_FOLDER + "bullet" + nf(i, 3) + ".png";
      bullet[i] = loadImage(filename);
    }
  }

  void shoot() {
    shot = true;
    bulletPos.x = noratoPos.x-bullet[0].width/2;
    bulletPos.y = noratoPos.y;
    velocity = shotVelocity;
  }

  void graveyard() {
    bulletPos = new PVector(grave.x, grave.y);
    velocity = zeroVelocity;
    shot = false;
  }

  void move() {
    velocity.normalize(); 
    velocity.mult(3);
    if (shot == true) {
      bulletPos.add(velocity);      
      if (bulletPos.x > width || bulletPos.y < 0 || bulletPos.y > height) {
        graveyard();
        shot = false;
      }
    }    
  }

  void display() {    
    if (shot == true) {
      frame = (frame+1) % imageCount; 
      noTint();
      image(bullet[frame], bulletPos.x, bulletPos.y);
    }
  }
}




private class BossBullet {
  PImage[] bullet;
  int imageCount;
  int frame;
  PVector velocity;
  protected PVector shotVelocity;
  private PVector bulletPos;
  boolean shot;

  BossBullet() {
    imageCount = 1;
    bullet = new PImage[imageCount];
    velocity = zeroVelocity;
    shotVelocity = new PVector(0, 3);
    bulletPos = new PVector(-100, -100 );


    for (int i = 0; i < imageCount; i++) {      
      String filename = FINAL_ASSET_FOLDER + "tirocaninana" + ".png";
      bullet[i] = loadImage(filename);
    }
  }

  void shoot(PVector bossPos) {    
    shot = true;
    bulletPos.x = bossPos.x-bullet[0].width/2;
    bulletPos.y = bossPos.y;
    velocity = shotVelocity;
  }

  void graveyard() {    
    bulletPos = new PVector(grave.x, grave.y);
    velocity = zeroVelocity;
    shot = false;
  }

  void move() {
    velocity.normalize();    
    velocity.mult(3);
    if (shot == true) {
      bulletPos.add(velocity);      
      if (bulletPos.x > width || bulletPos.y < 0 || bulletPos.y > height) {
        graveyard();
        shot = false;
      }
    }
  }

  void display() {    
    if (shot == true) {
      frame = (frame+1) % imageCount; 
      noTint();
      image(bullet[frame], bulletPos.x, bulletPos.y);
    }
  }
}