// classe para carregar frames e ordenar animação de sprites
class Animation {
  PImage[] images;
  PImage[] lifeSprites;
  PImage[] horns;
  int cor;
  int terra = 0xff236604;
  int terrainx;
  int terrainy;
  int frame;  
  boolean hornAnim = false;
  int hornFrame;
  int hornIdx;
  int lifes;
  int lifeY;
  boolean hit = false;



  Animation(String noratoImg, int noratoCount, String lifeImg, String hornName, int hornCount) {
    lifes = 3;
    images = new PImage[noratoCount];
    lifeSprites = new PImage[lifes];  
    horns = new PImage[hornCount];

    for (int i = 0; i < lifes; i++) {
      // Use nf() to number format 'i' into four digits
      String filename = FINAL_ASSET_FOLDER + lifeImg + ".png";
      lifeSprites[i] = loadImage(filename);
    }

    for (int i = 0; i < noratoCount; i++) {
      // Use nf() to number format 'i' into four digits
      String filename = FINAL_ASSET_FOLDER + noratoImg + nf(i, 3) + ".png";
      images[i] = loadImage(filename);
    }
    for (int i = 0; i < hornCount; i++) {
      // Use nf() to number format 'i' into four digits
      String filename = FINAL_ASSET_FOLDER + hornName + nf(i, 3) + ".png";
      horns[i] = loadImage(filename);
    }
  }

  public void display(PVector pos) {
    //cor = get(int(pos.x), int(pos.y));
    //hornFrame++;
    hornIdx = hornFrame/targetFps;
    frame++;
    int index = frame/targetFps;
    if (index >= images.length) {
      index = 0;
      frame = 0;
    }   



    if (invencible == true) {
      tint(255, 50);
    } else {
      noTint();
    }
    image(images[index], pos.x-norato.getWidth()/2, pos.y);

    image(horns[hornIdx], pos.x-norato.getWidth()/2, pos.y);

    if (hornAnim == true) {
      hornFrame++;
      if (hornIdx >= horns.length) {
        hornIdx = 0;
        frame = 0;        
        hornAnim = false;
      }
    }
  }

  public void hornAnimation() {
    //hornAnim = true;
  }

  public void lifeDisplay() {
    menus.menuTitle("vidas", 50, 25);
    
    int lifeY = 50;
    noTint();
    for (int N = 0; N < lifes; N++) {
      image(lifeSprites[N], 50, lifeY += 50);
    }

    if (lifes == 0) {
      start = false;
      gameOver = true;
    }
  }

  public void hitTest() {
    if (invencible == false) {
      if (hit == true) {        
        hit = false;
        lifes--;
        invencibilityTime = 0;
        invencible();
      }
    }
  }

  public void invencible() {
    if (chickenMode == false) {
      if (invencibilityTime <= invencibleFrames) {
        invencible = true;
        invencibilityTime++;
      } else {
        invencible = false;
      }
    } else {
      invencible = true;
    }
  }



  public void move(PVector noratoPos, float noratoSpd) {

    if (w == true && noratoPos.y >= 0+noratoSpd) {      
      noratoPos.y -= noratoSpd;
    }
    if (s == true && noratoPos.y <= height-100 /* && botCol == false*/) {
      noratoPos.y += noratoSpd;
    }     
    if (a == true && noratoPos.x >= 150+noratoSpd+(norato.getWidth()/2) /* && lefCol == false*/) {
      noratoPos.x -= noratoSpd;
    }     
    if (d == true && noratoPos.x <= width-150-noratoSpd-(norato.getWidth()/2) /* && rigCol == false*/) {
      noratoPos.x += noratoSpd;
    } 
    if (shift == true) {
      noratoSpd = 10;
    } else {
      noratoSpd = 3;
    }

    /*
    if (topCol == true) {
     //w = false;
     noratoPos.y += scnSpeed;
     }
     if (FLSlope == true) {
     //a = false;
     noratoPos.x += noratoSpd;
     }
     if (FRSlope == true) {
     //d = false;
     noratoPos.x -= noratoSpd;
     }
     if (topCol == true){
     noratoPos.y += noratoSpd+1;
     }
     if (lefCol == true){
     noratoPos.x += noratoSpd+1;
     }
     if (rigCol == true){
     noratoPos.x -= noratoSpd+1;
     }*/
  }

  public int getWidth() {
    return images[0].width;
  }
  public int getHeight() {
    return images[0].height;
  }
}