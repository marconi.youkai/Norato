// classe para carregar frames e ordenar animação de sprites

class Animation {
  PImage[] images;
  int imageCount;
  int frame;
  
  Animation(String imagePrefix, int count) {
    imageCount = count;
    images = new PImage[imageCount];

    for (int i = 0; i < imageCount; i++) {
      // Use nf() to number format 'i' into four digits
      String filename = "test assets/" + imagePrefix + nf(i, 3) + ".png";
      images[i] = loadImage(filename);
    }
  }

  void display(PVector pos) {
    //fra= fra %10;
    frame = ((frame+1) % imageCount);
    image(images[frame], pos.x-norato.getWidth()/2, pos.y);
  }
  
  void move(PVector noratoPos){
  if (key == 'w'){
    noratoPos.y = noratoPos.y - 3;
    println(noratoPos);
  }
  if (key == 'W' ){
    noratoPos.y = noratoPos.y - 10;
    println(noratoPos);
  }  
  if (key == 's'){
    noratoPos.y = noratoPos.y + 3;
    println(noratoPos);
  }
   if (key == 'S'){
    noratoPos.y = noratoPos.y + 10;
    println(noratoPos);
  }
  //noLoop();
}
  
  int getWidth() {
    return images[0].width;
  }
}