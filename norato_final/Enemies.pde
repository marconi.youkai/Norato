//classes de inimigos //<>// //<>// //<>//

abstract class Enemy {
  protected static final int BASE_SPEED = 4;
  boolean live = false;

  PVector position;
  PVector velocity;
  PImage[] images;
  PImage[] hornImg;
  int frame;
  int spawnDelay = int(random(300, 5000));
  int spawnTimer = int(random(100));

  Enemy() {    
    graveyard();
  }

  void Spawn() {
    if (scenePos <= height*scnLeng-scnSpeed) {
      if (spawnTimer >= spawnDelay) {
        live = true;
        position = new PVector (random(200, width-200), 0);
      }
    }
    spawnTimer++;
  }

  void graveyard() {    
    position = grave;
    velocity = zeroVelocity;
    spawnTimer = 0;
    live = false;    
    position = grave;
    velocity = zeroVelocity;
    spawnTimer = 0;
    live = false;
  }

  void loadSprites(String imgName, int imgCount) {
    images = new PImage[imgCount];
    for (int i = 0; i < imgCount; i++) {
      // Use nf() to number format 'i' into four digits
      String filename = FINAL_ASSET_FOLDER + imgName + nf(i, 3) + ".png";
      images[i] = loadImage(filename);
    }
  }

  void loadSprites(String imgName, int imgCount, String horn, int hornCount) {
    images = new PImage[imgCount];
    hornImg = new PImage[hornCount];

    for (int i = 0; i < imgCount; i++) {
      // Use nf() to number format 'i' into four digits
      String filename = FINAL_ASSET_FOLDER + imgName + nf(i, 3) + ".png";
      images[i] = loadImage(filename);
    }
    for (int i = 0; i < hornCount; i++) {
      // Use nf() to number format 'i' into four digits
      String filename = FINAL_ASSET_FOLDER + horn + nf(i, 3) + ".png";
      hornImg[i] = loadImage(filename);
    }
  }

  void move() {
    calculateVelocity();
    if (live == true) {
      position.add(velocity);
    }
    if (position.x > width - 150 || position.x < 150 || position.y < 0 || position.y > height) {
      graveyard();
    }
  }
  void display() {
    if (live == true) {
      frame++;
      int index = frame/targetFps;
      if (index >= images.length) {
        index = 0;
        frame = 0;
      }    
      noTint();
      image(images[index], position.x - images[0].width/2, position.y - images[0].height/2);
    }
  }
  abstract public void calculateVelocity();
}

class Piranha1 extends Enemy {

  Piranha1() {
    super();    
    velocity = new PVector(0, BASE_SPEED*3/4);
    loadSprites("piranha", 5);
  }    

  @Override
    public void calculateVelocity() {
    velocity = new PVector(0, BASE_SPEED*3/4);
  }
}

class Piranha2 extends Enemy { 
  float a = 0.0;
  float inc = TWO_PI/180.0;

  Piranha2() {
    super();
    loadSprites("piranhadois", 5);
  }

  @Override
    public void calculateVelocity() {     
    velocity = new PVector(sin(a), BASE_SPEED*5/8); 
    velocity.normalize(); 
    velocity.mult(2);
    a = a + inc;
  }
}

class Eel extends Enemy {
  float a = 0.0;
  float inc = TWO_PI/180.0;
  PVector eelPos;

  Eel() {
    eelPos = new PVector (width/2, 0);
    loadSprites("enguia", 9);
  }

  @Override
    public void calculateVelocity() {     
    velocity = new PVector(sin(a)*2, BASE_SPEED); 
    velocity.normalize(); 
    velocity.mult(4);
    velocity.y = 2;
    a = a + inc;
  }

  //@Override
  //  void move() {
  //  calculateVelocity();
  //  if (live == true) {
  //    eelPos.add(velocity);
  //  }
  //  if (eelPos.x > width || eelPos.y < 0 || eelPos.y > height) {
  //    graveyard();
  //  }
  //}
}


//classe baseada nos tutoriais de Daniel Shiffman 'https://processing.org:8443/tutorials/pvector/'
class Turtle extends Enemy { 
  boolean chase;

  Turtle() {
    super();
    loadSprites("tartaruga", 6);
    chase = true;
  }

  @Override
    public void calculateVelocity() {
    if (chase == true) {
      velocity = PVector.sub(noratoPos, position); 
      velocity.normalize(); 
      velocity.mult(2);
    }
  }

  @Override
    public void move() {
    super.move();    
    if (position.y >= noratoPos.y) {
      chase = false;
    }
    if (position.x <= 200 || position.x >= width-200) {
      position = grave;
      chase = true;
    }
  }
}

class Caninana extends Enemy {
  int life;
  private int invencibleTime;  
  boolean bossDead = false;
  boolean bossInvencible;
  int bulletDelay = 45;  
  int bulletTime = 0;
  PVector gun;  

  Caninana() {
    super();
    loadSprites("caninana", 10, "chifrescani", 1);
  }

  @Override
    public void calculateVelocity() {  
    velocity = PVector.sub(noratoPos, position);
    velocity.normalize(); 
    velocity.mult(5);
    velocity.y = 0;
    shooter();
  }

  @Override  
    void Spawn() {
    if (bossDead == false) {
      if (scenePos >= height*scnLeng-scnSpeed) {
        spawnCount = 1;
        life = 5;
        live = true;
        position = new PVector (random(200, width-200), 0);
        invencibleTime = 200;
      }
    }
  }  

  @Override
    void graveyard() {
    if (life <= 1) {
      position = grave;
      velocity = zeroVelocity;
      spawnTimer = 0;
      live = false;    
      position = grave;
      velocity = zeroVelocity;
      spawnTimer = 0;
      live = false;
      bossDead = true;
      if (bossDead == true && spawnCount >= 1) {        
        creditScreen = true;        
      }
    } else {
      if (bossInvencible == false) {
        life--;
        invencibleTime = 0;
      }
    }
  }

  private void invencibility() {
    if (invencibleTime <= invencibleFrames) {
      bossInvencible = true;
      invencibleTime++;
    } else {
      bossInvencible = false;
    }
  }

  @Override
    public void display() {
    if (live == true) {
      invencibility();
      frame++;
      int index = frame/targetFps;
      if (index >= images.length) {
        index = 0;
        frame = 0;
      }    
      if (bossInvencible == true) {
        tint(255, 50);
      } else {
        noTint();
      }
      image(images[index], position.x - images[0].width/2, position.y - images[0].height/2);
      image(hornImg[0], position.x - hornImg[0].width/2, position.y + 50);
    }
  }


  public void shooter() {   
    gun = new PVector(position.x, position.y);
    gun.y = gun.y + images[0].height/2;
    if (Math.abs(noratoPos.x - position.x) <= 50) {
      if (bulletTime >= bulletDelay) {
        for (BossBullet bossbullets : bossBullets) {
          if (bossbullets.shot == false) {   
            shootSE = true;
            menus.SE();
            bossbullets.shoot(gun);            
            bulletTime = 0;
            //bulletDelay = int(random(20, 80));
            shootSE = false;
            break;
          }
        }
      }
    }    
    bulletTime++;
  }
}