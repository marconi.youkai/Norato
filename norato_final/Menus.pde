//classe de menu bahaviors and styles //<>// //<>// //<>// //<>// //<>//

class Menus {
  PImage titleScreen;
  PImage logo;
  PImage startOpt;  
  PImage optionsOpt;  
  PImage exitOpt;
  PImage cursor;
  PImage paused;
  PVector cursorPos;
  PFont zelda;
  int fontZeldaSize = 50;
  int cursorSize = 30;  
  int optionMax;
  float optHeightTop;
  float optHeightBot;

  Menus() {
    titleScreen = loadImage(FINAL_ASSET_FOLDER + "capamenu" + ".png");
    logo = loadImage(FINAL_ASSET_FOLDER + "logo" + ".png");
    startOpt = loadImage(TEST_ASSET_FOLDER+ "start_opt" + ".png");
    optionsOpt = loadImage(TEST_ASSET_FOLDER+ "options_opt" + ".png");
    exitOpt = loadImage(TEST_ASSET_FOLDER+ "exit_opt" + ".png");
    cursor = loadImage(TEST_ASSET_FOLDER + "cursor" + ".png");
    paused = loadImage(TEST_ASSET_FOLDER + "pause" + ".png");
    creditsImg = loadImage(FINAL_ASSET_FOLDER + "creditos" + ".png");
    zelda = createFont("PiratesWriters.ttf", 50, false);
  }

  public void title() { 
    optionMax = 3;
    optHeightTop = height*1/2;
    optHeightBot = height*1/16;


    cursorMax = optionMax-1;

    image(titleScreen, 0, 0);
    image(logo, width/2 - logo.width/2 + 260, 65, logo.width/2, logo.height/2);
    startOpt(1);
    optionsOpt(2);    
    exitOpt(3);
    Music();
  }


  public void options() {
    optionMax = 3;
    cursorMax = optionMax - 1;
    optHeightTop = height*1/8;
    optHeightBot = height*1/16;    
    //image(optionsOpt, width/2 - paused.width/2, height*1/8 - paused.height/2);
    image(titleScreen, 0, 0);
    menuTitle("opções", width/2, height*1/8);
    soundOpt(1);
    chickenOpt(2);
    backOpt(3);
    Music();
  }


  public void pause(boolean pauseMenu) {
    cursorReset();
    if (pauseMenu == true) {
      cursorReset();
      fill(0, 100);
      rect(0, 0, width, height);
      menuTitle("paused", width/2, height/2);
      noLoop();
    } else {
      loop();
    }
  }

  public void gameOver() {   
    cursorReset();
    optionMax = 1;
    cursorMax = optionMax -1;
    optHeightTop = height*1/2;
    optHeightBot = height*7/8;
    background(0);        
    menuTitle("Game over", width/2, height/2);
    backOpt(1);    
    Music(); 
  }

  public void credits() {
    if (gameOver == false) {
      if (creditScreen == true) {      
        if (start == false) {          
          background(0);
          image(creditsImg, 0, creditsRoll);          
          start = false;
        }
      }
    }
  }

  public void startOpt(int index) {
    //cursorPos = new PVector(width/2 - startOpt.width + 20, height*3/4 - startOpt.height/2 + 10);
    float posX = width/2;
    float posY = ((height-optHeightTop-optHeightBot)*index/optionMax)+optHeightTop-optHeightBot;
    //image(startOpt, posX - startOpt.width/2, posY);
    menuTitle("Start", posX, posY);
    if (cursorIdx == index-1) {
      cursorDisplay(posX*5/8, posY);
      //image(cursor, posX - cursor.width, posY - 5, 30, 30);
      if (optSelect == true && start == false) {  
        SE();
        optSelect = false;        
        //select.play();
        //select.setGain(volume*50-50);
        //title.pause();
        //title.rewind();        
        titleMenu = false;        
        start = true;
        startTime = millis()/1000;        
        cursorIdx = -1;        
      }
    }
  }

  public void optionsOpt(int index) {
    //cursorPos = new PVector(width/2 - optionsOpt.width + 20, height*3/4 - optionsOpt.height/2 + 10);
    float posX = width/2;
    float posY = ((height-optHeightTop-optHeightBot)*index/optionMax)+optHeightTop-optHeightBot;
    //image(optionsOpt, posX - optionsOpt.width/2, posY);
    menuTitle("Options", posX, posY);    
    if (cursorIdx == index-1) {
      cursorDisplay(posX*5/8, posY);
      //image(cursor, posX - optionsOpt.width + 20, posY, startOpt.height, startOpt.height);
      if (optSelect == true) {
        SE();
        optSelect = false;
        optionsMenu = true;
        cursorReset();
      }
    }
  }

  public void exitOpt(int index) {
    //cursorPos = new PVector(width/2 - optionsOpt.width + 20, height*3/4 - optionsOpt.height/2 + 10);
    float posX = width/2;
    float posY = ((height-optHeightTop-optHeightBot)*index/optionMax)+optHeightTop-optHeightBot;
    //image(exitOpt, width/2 - exitOpt.width/2, height*7/8);
    menuTitle("Exit", posX, posY);
    if (cursorIdx == index-1) {
      cursorDisplay(posX*5/8, posY);
      //image(cursor, posX - exitOpt.width + 20, posY, startOpt.height, startOpt.height);
      if (optSelect == true) {
        SE();
        optSelect = false;
        delay(250);
        exit();
      }
    }
  } 

  public void soundOpt(int index) {
    float posTitleX = width*1/4;
    float posTitleY = ((height-optHeightTop-optHeightBot)*index/optionMax)+optHeightTop-optHeightBot;
    float posOptX = width*3/4;

    menuTitle("Som", posTitleX, posTitleY);
    if (sound == true) {
      menuTitle("on", posOptX, posTitleY);
    } else {
      menuTitle("off", posOptX, posTitleY);
    }    
    if (cursorIdx == index-1 && optionsMenu == true) {
      cursorDisplay(posTitleX*1/4, posTitleY);
      //image(cursor, posX - exitOpt.width + 20, posY, startOpt.height, startOpt.height);
      if (optSelect == true) {
        SE();
        optSelect = false;
        if (sound == true) {
          sound = false;
          Music();
          //title.pause();
          //title.rewind();
          //game.pause();
          //game.rewind();
        } else {
          sound = true;
        }
      }
    }
  }

  void chickenOpt(int index) {
    float posTitleX = width*1/4;
    float posTitleY = ((height-optHeightTop-optHeightBot)*index/optionMax)+optHeightTop-optHeightBot;
    float posOptX = width*3/4;
    menuTitle("chicken mode", posTitleX, posTitleY);
    if (chickenMode == true) {
      menuTitle("on", posOptX, posTitleY);
    } else {
      menuTitle("off", posOptX, posTitleY);
    }
    if (cursorIdx == index-1 && optionsMenu == true) {
      cursorDisplay(posTitleX*1/4, posTitleY);
      if (optSelect == true) {        
        norato.lifes = 3;
        if (chickenMode == false) {
          chickenMode = true;
          if (sound == true) {
            chicken.rewind();
            chicken.play();
          }
        } else {
          chickenMode = false;
          SE();
        }
        optSelect = false;
      }
    }
  }

  public void backOpt(int index) {
    float posTitleX = width*1/2;
    float posTitleY = ((height-optHeightTop-optHeightBot)*index/optionMax)+optHeightTop-optHeightBot;
    if (gameOver == true) {
      menuTitle("TitleScreen", posTitleX, height*3/4);
    } else {
      menuTitle("back", posTitleX, posTitleY);
    }

    if (cursorIdx == index-1) { 
      cursorDisplay(posTitleX*5/8, posTitleY);
      if (gameOver == true) {
        cursorDisplay(posTitleX*5/8, height*3/4);
      }
      if (optSelect == true) {
        SE();
        optSelect = false;  
        cursorReset();
        if (optionsMenu == true) {
          titleMenu = true;
          optionsMenu = false;
        }
        if (pauseMenu == true) {
          pauseMenu = false;
          loop();
        }
        if (gameOver == true) {
          creditScreen = false;
          gameOver = false;
          titleMenu = true;
          title();
          gameReset();
        }
      }
    }
  }
  public void cursorDisplay(float cursorX, float cursorY) {
    image(cursor, cursorX, cursorY - cursorSize/4, cursorSize, cursorSize);
  }

  public void cursorReset() {
    cursorIdx = 0;
  }

  public void menuTitle(String texto, float textX, float textY) {
    textFont(zelda);
    fill(0xff580101);
    textAlign(CENTER, CENTER);    
    text(texto, textX, textY);
  }

  void Music() {
    if (sound == true) {
      if (titleMenu == true || optionsMenu == true) {
        title.play();
      }
      if (start == true) {
        game.play();
      }
    } else {
      title.pause();
      title.rewind();
      game.pause();
      game.rewind();
    }
    title.setGain(volume*50-50);
    game.setGain(volume*50-50);
  }


  void SE() {
    if (sound == true) {
      if (optSelect == true) {
        select.rewind();
        select.play();
      }
    }  
    if (sound == true) {
      if (shootSE == true) {
        shoot.rewind();
        shoot.play();
      }
    }  
    if (sound == true) {
      if (hitSE == true) {
        hit.rewind();
        hit.play();
      }
    }  
    select.setGain(volume*50-50);
    chicken.setGain(volume*50-50);
  }
}