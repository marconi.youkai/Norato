//classe para carregar e montar os tiles do cen\u00e1rio de modo aleat\u00f3rio a cada jogo //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>//

class TileMount {  
  PImage[][] cenario;
  PImage[][] treeline;  
  //PImage[] frames;
  //PImage[] mounted;
  int targetFPS;
  int imageCount;
  int treeCount;
  int frame;
  int treeFrame;
  int order;

  TileMount(String imagePrefix, int count, int NTiles, String treePrefix, int treeFrames) {
    imageCount = count;    
    treeCount = treeFrames;
    order = PApplet.parseInt(random(5));
    cenario = new PImage[imageCount][NTiles];
    treeline = new PImage[treeCount][NTiles];    
    //frames = new PImage[imageCount];
    //mounted = new PImage[NTiles];    

    for (int linha = 0; linha < count; linha++) {      
      for (int coluna = 0; coluna < NTiles; coluna++) {
        String filename = FINAL_ASSET_FOLDER + imagePrefix + nf(linha, 2) + ".png"; 
        cenario[linha][coluna] = loadImage(filename);
      }
    }
    for (int linha = 0; linha < treeCount; linha++) {      
      for (int coluna = 0; coluna < NTiles; coluna++) {
        String filename = FINAL_ASSET_FOLDER + treePrefix + nf(linha, 2) + ".png"; 
        treeline[linha][coluna] = loadImage(filename);
      }
    }
  }


  public void display(int TileX, int TileY, int NTiles) {
    int fps = 20;
    int treeFps = 20;
    frame++;
    treeFrame ++;
    int sceneIndex = frame/fps;
    int treeIndex = treeFrame/treeFps;

    if (sceneIndex >= imageCount) {
      sceneIndex = 0;
      frame = 0;
    }

    if (treeIndex >= treeCount) {
      treeIndex = 0;
      treeFrame = 0;
    }
    noTint();
    for (int N = 0; N < NTiles; N++) {          
      image(cenario[sceneIndex][N], TileX, TileY -= 560);
    }    
    for (int N = 0; N < NTiles; N++) {          
      image(treeline[treeIndex][N], TileX, (TileY += 560) - 580);
      image(treeline[treeIndex][N], width-treeline[0][0].width, TileY - 580);
    }
  }
}

//class TileMount {
//  PImage[] tiles; 
//  PImage[] mounted; 
//  int imageCount; 
//  int frame; 
//  int order; 

//  TileMount(String imagePrefix, int count, int NTiles) {
//    imageCount = count; 
//    order = int(random(5)); 
//    tiles = new PImage[imageCount]; 
//    mounted = new PImage[NTiles]; 

//    for (int i = 0; i < imageCount; i++) {
//      // Use nf() to number format 'i' into four digits
//      String filename = FINAL_ASSET_FOLDER + imagePrefix + nf(i, 2) + ".png"; 
//      tiles[i] = loadImage(filename);
//    }
//    for (int O = 0; O < NTiles; O++) {
//      //order = int(random(imageCount)); 
//      mounted[O] = tiles[];
//    }
//  }

//  void display(int TileX, int TileY, int NTiles) {
//    for (int N = 0; N < NTiles; N++) {
//      image(mounted[N], TileX, TileY -= 560 );
//    }
//  }
//}









//class TileMount {
//  PImage[] tiles; 
//  PImage[] mounted; 
//  int imageCount; 
//  int frame; 
//  int order; 

//  TileMount(String imagePrefix, int count, int NTiles) {
//    imageCount = count; 
//    order = int(random(5)); 
//    tiles = new PImage[imageCount]; 
//    mounted = new PImage[NTiles]; 

//    for (int i = 0; i < imageCount; i++) {
//      // Use nf() to number format 'i' into four digits
//      String filename = TEST_ASSET_FOLDER + imagePrefix + nf(i, 3) + ".png"; 
//      tiles[i] = loadImage(filename);
//    }
//    for (int O = 0; O < NTiles; O++) {
//      order = int(random(imageCount)); 
//      mounted[O] = tiles[order];
//    }
//  }

//  void display(int TileX, int TileY, int NTiles) {
//    for (int N = 0; N < NTiles; N++) {
//      image(mounted[N], TileX, TileY -= 560 );
//    }
//  }
//}