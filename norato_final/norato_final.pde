import ddf.minim.*; //<>// //<>// //<>// //<>// //<>// //<>//
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;
import javax.sound.sampled.Control;
import java.awt.event.KeyEvent; 


//import processing.sound.*;
//SoundFile title, game, credits, select;
Minim minim;
AudioPlayer title, game, credits, select, chicken, hit, shoot;
Control titleVolume, gameVolume, creditsVolume, selectVolume, chickenVolume, hitVolume, shootVolume;

Animation norato, move;
TileMount Tiles;
Bullet[] bullets;
BossBullet[] bossBullets;
Menus menus;
Enemy[] enemies;

static final String TEST_ASSET_FOLDER = "data/test assets/";
static final String FINAL_ASSET_FOLDER = "data/final assets/";
static final int targetFps = 5;

//controle de menus
int cursorIdx = 0;
int cursorMax;
boolean optSelect = false;
boolean chickenMode = false;

//booleans que indicam menus a serem exibidos
boolean titleMenu = true;
boolean optionsMenu = false;
boolean pauseMenu = false;
boolean start = false;
boolean gameOver = false;
boolean creditScreen = false;

//music toggle booleans

//music configs
float volume = 0.5;
boolean sound = true;


//color testing
/*
color pointer;
 color frontL;
 color frontR;
 color backL;
 color backR;
 color TL;
 color TR;
 color BL;
 color BR;
 color agua = #0a74c0;
 color terra = #236604;
 boolean topCol;
 boolean lefCol;
 boolean rigCol;
 boolean botCol;
 boolean FLSlope;
 boolean FRSlope;
 boolean BLSlope;
 boolean BRSlope;
 */

PImage Cenario;
int startTime;
int time;
int scenePos;// posição do cenário
int scnLeng = 15; // variavel para controlar o compriment do cenário em numero de tiles
int scnSpeed = 0; // controla a velocidade da movimentação do cenário

//controle posicional norato, valores declarados como default antes de mouse over
PVector noratoDist;
PVector noratoPos;
int invencibleFrames = 120;
int invencibilityTime = 200;
boolean invencible = false;
float noratoSpd = 3;
PVector mouse;
PVector speed = new PVector(0, 0);
PVector accel = new PVector(0, 0);

boolean w;
boolean a;
boolean s;
boolean d;
boolean spacebar = false;
boolean pause = false;
boolean shift;

/*
float xpos = 280 ;
 float ypos = 444 ;
 float drag = 5; //ajuste de aceleração do mouse
 */

//controle posicional de tiles
int TileX;
int TileY;


//enemy counts
int piranha1Count = 10;
int piranha2Count = 10;
int eelCount = 10;
int turtleCount = 10;
int bossCount = 1;

//controle de posição de tiro
static PVector grave = new PVector(-300, -300);
PVector zeroVelocity = new PVector(0, 0);
PVector bulletDist  = new PVector(0, 0);
PVector bossBulletDist = new PVector(0, 0);
int frameDelay = 20;
int currentFrame = 50;
boolean shootSE = false;
boolean hitSE = false;

PImage creditsImg;
float creditsRoll = 0;
int creditsTime;
int creditsdelay = 100; 
int spawnCount = 0;


void setup() {
  size(820, 560);
  frameRate(60); // ajusta framerate do jogo para 6 frames afim de criar a ilusão de um jogo antigo
  //noSmooth();    
  scenePos = 560;
  noratoPos = new PVector(width/2, height*3/4);
  norato = new Animation("norato", 10, "life", "chifres", 12);
  Tiles = new TileMount("cenario", 6, scnLeng, "treeline", 2);
  bullets = new Bullet[10];
  bossBullets = new BossBullet[10];
  menus = new Menus();
  startTime = millis()/1000;  

  //sound files load
  minim = new Minim(this);
  title = minim.loadFile("Boss loop.mp3");
  game = minim.loadFile("BGM LOOP_mixdown.mp3");
  select = minim.loadFile("select.mp3");
  chicken = minim.loadFile("chicken.mp3");
  hit = minim.loadFile("hit.mp3");
  shoot = minim.loadFile("tiro.mp3");
  //credits = new SoundFile(this, "credits.mp3");
  //titleVolume = title.getControl(Controller.VOLUME);
  //gameVolume = game.getControl(controller.VOLUME);.
  enemies = new Enemy[piranha1Count+piranha2Count+eelCount+turtleCount+bossCount];
  int enemyIndex = 0;
  for (int i = 0; i < piranha1Count; i++, enemyIndex++) {
    enemies[enemyIndex] = new Piranha1();
  }
  for (int i = 0; i < piranha2Count; i++, enemyIndex++) {
    enemies[enemyIndex] = new Piranha2();
  }
  for (int i = 0; i < eelCount; i++, enemyIndex++) {
    enemies[enemyIndex] = new Eel();
  }
  for (int i = 0; i < turtleCount; i++, enemyIndex++) {
    enemies[enemyIndex] = new Turtle();
  }
  for (int i = 0; i < bossCount; i++, enemyIndex++) {
    enemies[enemyIndex] = new Caninana();
  }
  for (int i = 0; i < bullets.length; i++) {
    bullets[i] = new Bullet();
  }
  for (int i = 0; i < bossBullets.length; i++) {
    bossBullets[i] = new BossBullet();
  }
}

/*void mouseMoved(){ // controles para movimentação do personagem usando o mouse
 
 mouse = new PVector(mouseX, mouseY);
 //mouse.sub(noratoPos);
 //mouse.setMag(0.002);
 //noratoPos.limit(5);
 //speed = mouse;
 //speed.limit(1);
 //noratoPos.add(speed);
 noratoPos = mouse;*/

/*float dx = mouseX - xpos;
 float dy = mouseY - ypos;
 xpos = xpos + dx/drag;
 ypos = ypos + dy/drag;
 }*/

public void keyPressed() {
  if (start == true) {
    //melhor usar o codigo numerico ao inves de checkar pela letra minuscula e maiuscula em cada if()
    //para consulta rapida dos codigos acessar http://keycode.info/
    // 87 = w
    if (keyCode == KeyEvent.VK_W || keyCode == KeyEvent.VK_UP/* && noratoPos.y >= 0+noratoSpd && topCol == false && FLSlope == false && FRSlope == false*/) {
      w = true;
    }
    // 83 = s
    if (keyCode == KeyEvent.VK_S ||keyCode == KeyEvent.VK_DOWN/* && noratoPos.y <= height-50 && botCol == false && BLSlope == false && BRSlope == false*/) {
      s = true;
    }  
    // 65 = a
    if (keyCode == KeyEvent.VK_A || keyCode == KeyEvent.VK_LEFT/* && noratoPos.x >= 0+noratoSpd && lefCol == false && FLSlope == false && BLSlope == false*/) {
      a = true;
    }  
    // 68 = d
    if (keyCode == KeyEvent.VK_D || keyCode == KeyEvent.VK_RIGHT /* && noratoPos.x <= width-noratoSpd && rigCol == false && FRSlope == false && BRSlope == false*/) {
      d = true;
    }
    // 80 = p
    if (keyCode == KeyEvent.VK_P) {
      if (looping) {
        pause = true;          
        menus.pause(pause);
      } else {
        pause = false;
        menus.pause(pause);
      }
    }

    if (keyCode == KeyEvent.VK_K) {
      if (chickenMode == false) {
        norato.lifes--;
      }
    }

    // 32 = SPACEBAR
    if (keyCode == KeyEvent.VK_SPACE) {      
      norato.hornAnimation();      
      for (Bullet bullets : bullets) {
        if (bullets.shot == false) {
          if (currentFrame >= frameDelay) {
            shootSE = true;
            menus.SE();
            bullets.shoot();
            currentFrame = 0;
            shootSE = false;
            break;
          }
        }
      }
    }
  }


  // 16 = shift
  if (keyCode == 16) {
    shift = true;
  }

  // 38 = up || 87 = w
  if (keyCode == KeyEvent.VK_W || keyCode == KeyEvent.VK_UP) {
    if (cursorIdx >= 1) {
      cursorIdx--;
    }
  }
  // 40 = down || 83 = s
  if (keyCode == KeyEvent.VK_S || keyCode == KeyEvent.VK_DOWN) {
    if (cursorIdx < cursorMax) {
      cursorIdx++;
    }
  }
  // 13 = enter
  if (keyCode == KeyEvent.VK_ENTER) {
    optSelect = true;
    if (pauseMenu == true) {
      loop();
      optSelect = false;
    }
  }
}

public void keyReleased() { 
  // 87 = w
  if (keyCode == KeyEvent.VK_W || keyCode == KeyEvent.VK_UP /*|| noratoPos.y <= 0+noratoSpd /* && topCol == true && FLSlope == true && FRSlope == true*/) {
    w = false;
  }
  // 83 = s
  if (keyCode == KeyEvent.VK_S || keyCode == KeyEvent.VK_DOWN /*|| noratoPos.y >= height-50 /* && botCol == true && BLSlope == true && BRSlope == true*/) {
    s = false;
  }
  // 65 = a
  if (keyCode == KeyEvent.VK_A || keyCode == KeyEvent.VK_LEFT /*|| noratoPos.x <= 0+noratoSpd /* && lefCol == true && FLSlope == true && BLSlope == true*/) {
    a = false;
  }
  // 68 = d
  if (keyCode == KeyEvent.VK_D || keyCode == KeyEvent.VK_RIGHT /*|| noratoPos.x <= width-noratoSpd /* && rigCol == true && FRSlope == true && BRSlope == true*/) {
    d = false;
  }
  // 16 = shift
  if (keyCode == 16) {
    shift = false;
  }
}

void draw() {
  noCursor();
  background(154);
  time = millis()/1000 - startTime;
  if (titleMenu == true) {
    start = false;
    optionsMenu = false;
    pauseMenu = false;
    menus.title();
  }

  if (optionsMenu == true) {
    start = false;
    titleMenu = false;
    pauseMenu = false;
    menus.options();
  }

  if (gameOver == true) {
    menus.gameOver();
    for (Enemy enemy : enemies) {
      enemy.graveyard();
    }
  }  

  if (start == true) {
    title.pause();

    if (sound == true) {
      game.play();
      game.setGain(volume*50-50);
    } else {      
      game.pause();
    }

    Tiles.display(0, scenePos, scnLeng);

    for (Enemy enemy : enemies) {
      if (enemy.live == true) {
        enemy.move();
        enemy.display();
      } else {
        enemy.Spawn();
      }
    }    

    currentFrame++;
    for (Bullet bullets : bullets) {
      bullets.move();
      bullets.display();
    }

    for (BossBullet bossbullets : bossBullets) {           
      bossbullets.move();
      bossbullets.display();
    }

    for (BossBullet bossbullets : bossBullets) {  
      if ( bossbullets.shot == true) { 
        bossBulletDist = PVector.sub(bossbullets.bulletPos, noratoPos);
        if (bossBulletDist.mag() <= 30) {
          norato.hit = true;
          hitSE = true;           
          norato.hitTest();
          bossbullets.graveyard();
          menus.SE();
          hitSE= false;
        }
      }
    }



    for (Bullet bullets : bullets) {  
      if ( bullets.shot == true) { 
        for (Enemy enemy : enemies) {
          if (enemy.live == true) {
            bulletDist = PVector.sub(bullets.bulletPos, enemy.position);
            if (bulletDist.mag() <= 30) {
              hitSE = true;
              enemy.graveyard();
              bullets.graveyard();
              menus.SE();
              hitSE= false;
            }
          }
        }
      }
    }

    for (Enemy enemy : enemies) {
      if (enemy.live == true) {
        noratoDist = PVector.sub(noratoPos, enemy.position);
        if (invencible == false) {
          if (noratoDist.mag() <= 30) {
            norato.hit = true;
            hitSE = true;
            norato.hitTest();
            enemy.graveyard();
            menus.SE();
            hitSE = false;
          }
        }
      }
    }

    if (gameOver == true) {
      for (Enemy enemy : enemies) {
        enemy.graveyard();
      }
      for (Bullet bullets : bullets) {
        bullets.graveyard();
      }
      for (BossBullet bossbullets : bossBullets) {
        bossbullets.graveyard();
      }
    }

    norato.display(noratoPos);
    norato.lifeDisplay();
    norato.invencible();    

    /*
    frontL = get(int(noratoPos.x - norato.getWidth()/2), int(noratoPos.y - 3));
     frontR = get(int(noratoPos.x + norato.getWidth()/2), int(noratoPos.y - 3));
     backL = get(int(noratoPos.x), int(noratoPos.y +3));
     backR = get(int(noratoPos.x), int(noratoPos.y +3));
     TL = get(int(noratoPos.x - norato.getWidth()/2), int(noratoPos.y));
     TR = get(int(noratoPos.x + norato.getWidth()/2), int(noratoPos.y));
     BL = get(int(noratoPos.x - norato.getWidth()/2), int(noratoPos.y+norato.getHeight()/3));
     BR = get(int(noratoPos.x + norato.getWidth()/2), int(noratoPos.y + norato.getHeight()/3));
     
     if (frontL == terra && TL == terra) {    
     FLSlope = true;
     } else {
     FLSlope = false;
     }
     if (frontR == terra && TR == terra) {
     FRSlope = true;
     } else {
     FRSlope = false;
     }
     if (backL == terra && BL == terra) {
     BLSlope = true;
     } else {
     BLSlope = false;
     }
     if (backR == terra && BR == terra) {
     BRSlope = true;
     } else {
     BRSlope = false;
     }
     if (TL == terra && TR == terra) {
     topCol = true;
     } else {
     topCol = false;
     }
     if (TL == terra && BL == terra) {
     lefCol = true;
     } else {
     lefCol = false;
     }
     if (TR == terra && BR == terra) {
     rigCol = true;
     } else {
     rigCol = false;
     }
     if (BL == terra && BR == terra) {
     botCol = true;
     } else {
     botCol = false;
     }
     */

    norato.move(noratoPos, noratoSpd);

    if (pause == false) {
      if (time >= 3) {
        scnSpeed = 4;
        if (scenePos <= height*scnLeng-scnSpeed) {
          //image(Cenario, 0, ScenePos = ScenePos + 15); // a cada frame o cen\u00e1rio \u00e9 avan\u00e7ado em um incremento de 4 px
          scenePos = scenePos + scnSpeed;
        }
      }
    }
  }
  if (creditScreen == true) {
    start = false;
    menus.credits();
    if (creditsTime <= creditsdelay) {
      creditsTime++;
    } else if (creditsRoll >= -creditsImg.height) {
      creditsRoll -= 1;    
      println(creditsRoll);
    } else {      
      exit();
    }
  }
}

void gameReset() {
  spawnCount = 0;
  norato.lifes = 3;
  noratoPos = new PVector(width/2, height*3/4);
  scenePos = 560;
  invencibilityTime = 200;
  for (Enemy enemy : enemies) {
    enemy.graveyard();
  }
  for (Bullet bullets : bullets) {
    bullets.graveyard();
  }
  for (BossBullet bossbullets : bossBullets) {
    bossbullets.graveyard();
  }
}